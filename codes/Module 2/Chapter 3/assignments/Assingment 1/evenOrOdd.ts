function check1(): void {
    //getting data from user
    var nums : HTMLInputElement = <HTMLInputElement>document.getElementById("num");
    //parsing data
    var num : number = +nums.value;

    //validating the data
    if(isNaN(num)) {
        alert("Please enter Valid data");
    }
    else{                   //correcting the data
        if(num < 0) {               
            num = Math.abs(num);                //if number is negetive taking absolute value
        }
        if(num%2 == 0) {
            alert("The Entered number is Even");            //even number answer
        }
        else{
            alert("The Entered number is Odd");                 //odd number answer
        }
    }
}