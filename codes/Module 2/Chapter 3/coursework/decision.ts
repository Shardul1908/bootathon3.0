function check() {
    var a : HTMLInputElement = <HTMLInputElement>document.getElementById("t1");
    var data : number = +a.value;
    if(isNaN(data)){
        alert("NOT A NUMBER");
    }
    else{
        document.getElementById("display").innerHTML = data.toString(); 
    }
}

function minmax() {
    var x1 : HTMLInputElement = <HTMLInputElement>document.getElementById("x");
    var y1 : HTMLInputElement = <HTMLInputElement>document.getElementById("y");
    var x : number = +x1.value;
    var y : number = +y1.value;
    if(x<y) {
        alert("x is lesser than y");
    }
    else if(x == y) {
        alert("x is equal to y");
    }
    else if(x>y){
        alert("x is greater than y");
    }
}



function type1() {
    alert("hi");
    var ss1 : HTMLInputElement = <HTMLInputElement>document.getElementById("s1");
    var ss2 : HTMLInputElement = <HTMLInputElement>document.getElementById("s2");
    var ss3 : HTMLInputElement = <HTMLInputElement>document.getElementById("s3");

    var s1 : number = +ss1.value;
    var s2 : number = +ss2.value;
    var s3 : number = +ss3.value;

    if((s1==s2) && (s2==s3)) {
        document.getElementById("display").innerHTML = "The triangle is a equilateral triangle";
    }
    else if((s1==s2) && (s1!=s3)){
        document.getElementById("display").innerHTML = "The triangle is a isosceles triangle";
    }
    else{
        document.getElementById("display").innerHTML = "The triangle is a scalene triangle";
    }
}


function complex() {
    var complex : HTMLInputElement = <HTMLInputElement>document.getElementById("c");

    var real : HTMLInputElement = <HTMLInputElement>document.getElementById("c1");
    var img : HTMLInputElement = <HTMLInputElement>document.getElementById("c2");

    var data : string = complex.value;

    var realnum : number;
    var imgnum : number;

    var i : number = data.indexOf("+");
    if(i != -1) {
        realnum = parseFloat(data.substring(0,i));
        imgnum = parseFloat(data.substring(i+1,data.length));

        real.value = realnum.toString();
        img.value = imgnum.toString();
    }
}