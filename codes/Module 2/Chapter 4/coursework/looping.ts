function FOR() {
    var count : number;
    for(count = 1;count<=100;count++) {
        console.log(count);
    }
}

function WHILE() : void {
    var count : number;
    count = 200;
    while (count>100) {
        console.log(count);
        count--;
    }
}


function DO_WHILE():void {
    var count : number;
    count = 200;
    do {
        console.log(count);
        count--;
    }while(count>100);
}

function FACT():void {
    var nums : HTMLInputElement = <HTMLInputElement>document.getElementById("text1"); 
    var disp : HTMLInputElement = <HTMLInputElement>document.getElementById("display");

    var num : number = parseFloat(nums.value);
    var fact : number = 1;
    while(num > 0) {
        fact *= num;
        num--;
    }

    disp.innerHTML = fact.toString();
}

function DYNAMIC() : void {
    var input:HTMLInputElement = <HTMLInputElement>document.getElementById("text2");
    var  table:HTMLTableElement = <HTMLTableElement>document.getElementById("table");

    var count : number = 1;

    while (table.rows.length > 1) {
        table.deleteRow(1);
    }
    var Num : number = parseFloat(input.value);

    for(count = 1;count<=Num;count++) {
        var row : HTMLTableRowElement = table.insertRow();
        var cell : HTMLTableDataCellElement = row.insertCell();

        var text : HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = count.toString();
        cell.appendChild(text);

        var cell:HTMLTableDataCellElement = row.insertCell();

        var text : HTMLInputElement = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = (count*count).toString();
        cell.appendChild(text);


    }
}