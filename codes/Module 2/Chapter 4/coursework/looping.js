function FOR() {
    var count;
    for (count = 1; count <= 100; count++) {
        console.log(count);
    }
}
function WHILE() {
    var count;
    count = 200;
    while (count > 100) {
        console.log(count);
        count--;
    }
}
function DO_WHILE() {
    var count;
    count = 200;
    do {
        console.log(count);
        count--;
    } while (count > 100);
}
function FACT() {
    var nums = document.getElementById("text1");
    var disp = document.getElementById("display");
    var num = parseFloat(nums.value);
    var fact = 1;
    while (num > 0) {
        fact *= num;
        num--;
    }
    disp.innerHTML = fact.toString();
}
function DYNAMIC() {
    var input = document.getElementById("text2");
    var table = document.getElementById("table");
    var count = 1;
    while (table.rows.length > 1) {
        table.deleteRow(1);
    }
    var Num = parseFloat(input.value);
    for (count = 1; count <= Num; count++) {
        var row = table.insertRow();
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = count.toString();
        cell.appendChild(text);
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.value = (count * count).toString();
        cell.appendChild(text);
    }
}
//# sourceMappingURL=looping.js.map