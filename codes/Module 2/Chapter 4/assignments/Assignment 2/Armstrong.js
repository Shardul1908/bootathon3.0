function getNumbers() {
    //starting from number 1
    var i = 1;
    while (i <= 500) { //going till 500
        var num = i; //duplicating the number
        var sum = 0; //initialize sum to zero
        while (num > 0) {
            var d = num % 10; //getting digits from the number
            sum += (d * d * d); //cubing
            num = num / 10; //dividing by 10
            num = parseInt(num.toString());
        }
        if (sum == i) { //Condition for armstrong number
            document.getElementById("disp").innerHTML += i.toString() + "<br>"; //printing a armstrong number
        }
        i++;
    }
}
//# sourceMappingURL=Armstrong.js.map