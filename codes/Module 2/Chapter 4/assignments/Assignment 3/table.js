function getTable() {
    //getting input
    var input = document.getElementById("num");
    //getting table
    var table = document.getElementById("tab");
    //parsing to number
    var num = +input.value;
    if (isNaN(num)) { //validating the data
        alert("Enter Valid data");
    }
    else { //correct data
        var count;
        while (table.rows.length > 1) { //erasing the table till it has only headers left
            table.deleteRow(1);
        }
        for (count = 1; count <= num; count++) { //running for num times
            var row = table.insertRow(); //creating a row
            var cell = row.insertCell(); //insert cell in row
            var text = document.createElement("input"); //creating element 
            text.type = "text";
            text.style.textAlign = "center"; //aligning text to centre
            text.value = num.toString(); //filling up the text with data
            cell.appendChild(text); //putting the text field in the cell
            //same procedure a above but different data passed to the value of the text
            var cell = row.insertCell();
            var text = document.createElement("input");
            text.type = "text";
            text.style.textAlign = "center";
            text.value = "x";
            cell.appendChild(text);
            var cell = row.insertCell();
            var text = document.createElement("input");
            text.type = "text";
            text.style.textAlign = "center";
            text.value = count.toString();
            cell.appendChild(text);
            var cell = row.insertCell();
            var text = document.createElement("input");
            text.type = "text";
            text.style.textAlign = "center";
            text.value = "=";
            cell.appendChild(text);
            var cell = row.insertCell();
            var text = document.createElement("input");
            text.type = "text";
            text.style.textAlign = "center";
            text.value = (num * count).toString();
            cell.appendChild(text);
        }
    }
}
//# sourceMappingURL=table.js.map