function getTable() : void {
    //getting input
    var input:HTMLInputElement = <HTMLInputElement>document.getElementById("num");
    //getting table
    var  table:HTMLTableElement = <HTMLTableElement>document.getElementById("tab");

    //parsing to number
    var num : number = +input.value;
    if(isNaN(num)) {                //validating the data
        alert("Enter Valid data");
    }
    else{                                   //correct data
        var count : number;
        while (table.rows.length > 1) {                             //erasing the table till it has only headers left
            table.deleteRow(1);

        }
        for(count = 1;count<=num;count++) {                             //running for num times
            var row : HTMLTableRowElement = table.insertRow();              //creating a row

            var cell : HTMLTableDataCellElement = row.insertCell();             //insert cell in row
            var text : HTMLInputElement = document.createElement("input");          //creating element 
            text.type = "text";
            text.style.textAlign = "center";                        //aligning text to centre
            text.value = num.toString();                            //filling up the text with data
            cell.appendChild(text);                                //putting the text field in the cell

            //same procedure a above but different data passed to the value of the text
            var cell : HTMLTableDataCellElement = row.insertCell();
            var text : HTMLInputElement = document.createElement("input");
            text.type = "text";
            text.style.textAlign = "center";
            text.value = "x";                                                   
            cell.appendChild(text);

            var cell : HTMLTableDataCellElement = row.insertCell();
            var text : HTMLInputElement = document.createElement("input");
            text.type = "text";
            text.style.textAlign = "center";
            text.value = count.toString();
            cell.appendChild(text);

            var cell : HTMLTableDataCellElement = row.insertCell();
            var text : HTMLInputElement = document.createElement("input");
            text.type = "text";
            text.style.textAlign = "center";
            text.value = "=";
            cell.appendChild(text);

            var cell : HTMLTableDataCellElement = row.insertCell();
            var text : HTMLInputElement = document.createElement("input");
            text.type = "text";
            text.style.textAlign = "center";
            text.value = (num*count).toString();
            cell.appendChild(text);
        }
    }
}