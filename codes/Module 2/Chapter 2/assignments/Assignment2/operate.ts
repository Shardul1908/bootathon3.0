function operate1() : void {
    //getting the data
    var str : HTMLInputElement = <HTMLInputElement>document.getElementById("txt");
    //making string
    var str1 : string = str.value

    document.getElementById("Answer1").innerHTML += "<b>" + str1.toUpperCase() + "</b>";            //printing upperCase
    document.getElementById("Answer2").innerHTML += "<b>" + str1.toLowerCase() + "</b>";            //printing lowerCase

    var arr : string[] = [];

    arr = str1.split(" ");              //splitting the string ar spaces
    var i : number;
    for(i=0;i<arr.length;i++) {
        document.getElementById("Answer3").innerHTML += (i+1).toString() + ") " + arr[i] + "<br>";      //Printing array contents
    }
}