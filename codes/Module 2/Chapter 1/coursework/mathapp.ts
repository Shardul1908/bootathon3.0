function Sine() {
    let t11 : HTMLInputElement = <HTMLInputElement>document.getElementById("t11");
    let t12 : HTMLInputElement = <HTMLInputElement>document.getElementById("t12");
    var a = Math.sin(parseFloat(t11.value)*(Math.PI/180))
    t12.value = a.toString();
}

function CoSine() {
    let t21 : HTMLInputElement = <HTMLInputElement>document.getElementById("t21");
    let t22 : HTMLInputElement = <HTMLInputElement>document.getElementById("t22");
    var a = Math.cos(parseFloat(t21.value)*(Math.PI/180))
    t22.value = a.toString();
}