function area() {
    //Getting Input from the user
    var x1s = document.getElementById("x1");
    var y1s = document.getElementById("y1");
    var x2s = document.getElementById("x2");
    var y2s = document.getElementById("y2");
    var x3s = document.getElementById("x3");
    var y3s = document.getElementById("y3");
    //converting into numbers
    var x1 = parseFloat(x1s.value);
    var x2 = parseFloat(x2s.value);
    var x3 = parseFloat(x3s.value);
    var y1 = parseFloat(y1s.value);
    var y2 = parseFloat(y2s.value);
    var y3 = parseFloat(y3s.value);
    //Validating the data entered by the user
    if (isNaN(x1) || isNaN(x2) || isNaN(x3) || isNaN(y1) || isNaN(y2) || isNaN(y3)) {
        alert("Please Enter Valid Data");
    }
    else { //correct data entered
        var a = Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2));
        var b = Math.sqrt(Math.pow((x1 - x3), 2) + Math.pow((y1 - y3), 2)); //Calculations for length of the sides
        var c = Math.sqrt(Math.pow((x3 - x2), 2) + Math.pow((y3 - y2), 2));
        var s = (a + b + c) / 2; //Calculating s
        console.log(s);
        var area = Math.sqrt((s) * (s - a) * (s - b) * (s - c)); //Calculating area
        console.log(area);
        var ans = document.getElementById("ans");
        ans.value = area.toString(); //Printing area in ans text box
    }
}
//# sourceMappingURL=area.js.map