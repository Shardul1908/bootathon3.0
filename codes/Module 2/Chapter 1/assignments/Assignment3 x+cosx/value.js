function value1() {
    //getting input
    var nums = document.getElementById("num");
    //Parsing the data
    var num = +nums.value;
    //Validating the data
    if (isNaN(num)) {
        alert("Please Enter valid Data");
    }
    else { //Correct data
        var ans;
        var cos = Math.cos(num * (Math.PI / 180));
        ans = num + cos; //Calculations
        var answer = document.getElementById("val");
        answer.value = ans.toString(); //Printing answer
    }
}
//# sourceMappingURL=value.js.map