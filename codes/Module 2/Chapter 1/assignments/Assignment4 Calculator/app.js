var text = document.getElementById("result");
var operation;
//This funtion assigns the values to operation variable and also creates a string entered by the user
function clicked(a) {
    text.value += a;
    if (a == "+") {
        operation = "+"; //tells calculator addition is to be performed
    }
    else if (a == "-") {
        operation = "-"; //tells calculator substraction is to be performed 
    }
    else if (a == "*") {
        operation = "*"; //tells calculator multiply is to be performed
    }
    else if (a == "/") {
        operation = "/"; //tells calculator divide is to be performed
    }
    else if (a == "sin(") {
        alert("Remember to the end the function with )");
        operation = "sin"; //tells calculator Sine is to be performed
    }
    else if (a == "cos(") {
        alert("Remember to the end the function with )");
        operation = "cos"; //tells calculator Cosine is to be performed
    }
    else if (a == "tan(") {
        alert("Remember to the end the function with )");
        operation = "tan"; //tells calculator Tangent is to be performed
    }
    else if (a == "sqrt(") {
        alert("Remember to the end the function with )");
        operation = "sqrt"; //tells calculator Square root is to be performed
    }
    else if (a == "pow(") {
        alert("Remember to the end the function with )");
        operation = "pow"; //tells calculator Power is to be performed
    }
}
//Solves the expression Entered by the user
function solve() {
    var result = 0;
    var str = text.value;
    if (operation == "+") {
        var a = str.split("+", 2);
        var num1 = parseFloat(a[0]);
        var num2 = parseFloat(a[1]);
        result = num1 + num2; //addition
    }
    else if (operation == "-") {
        var a = str.split("-", 2);
        var num1 = parseFloat(a[0]);
        var num2 = parseFloat(a[1]);
        result = num1 - num2; //substract
    }
    else if (operation == "*") {
        var a = str.split("*", 2);
        var num1 = parseFloat(a[0]);
        var num2 = parseFloat(a[1]);
        result = num1 * num2; //multiply
    }
    else if (operation == "/") {
        var a = str.split("/", 2);
        var num1 = parseFloat(a[0]);
        var num2 = parseFloat(a[1]);
        result = num1 / num2; //divide
    }
    else if (operation == "sin") {
        var pos = str.indexOf("(");
        var b = str.substring(pos + 1, str.length - 1);
        var num = parseFloat(b);
        result = Math.sin(num * (Math.PI / 180)); //sin value
    }
    else if (operation == "cos") {
        var pos = str.indexOf("(");
        var b = str.substring(pos + 1, str.length - 1);
        var num = parseFloat(b);
        result = Math.cos(num * (Math.PI / 180)); //cos value
    }
    else if (operation == "tan") {
        var pos = str.indexOf("(");
        var b = str.substring(pos + 1, str.length - 1);
        var num = parseFloat(b);
        result = Math.tan(num * (Math.PI / 180)); //tan value
    }
    else if (operation == "sqrt") {
        var pos = str.indexOf("(");
        var b = str.substring(pos + 1, str.length - 1);
        var num = parseFloat(b);
        result = Math.sqrt(num); //square root
    }
    else if (operation == "pow") {
        var pos = str.indexOf("(");
        var b = str.substring(pos + 1, str.length - 1);
        var a = b.split(",");
        var num1 = parseFloat(a[0]);
        var num2 = parseFloat(a[1]);
        result = Math.pow(num1, num2); //power function
    }
    text.value = result.toString();
}
//# sourceMappingURL=app.js.map