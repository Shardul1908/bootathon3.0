var text : HTMLInputElement = <HTMLInputElement>document.getElementById("result");
var operation : string;

//This funtion assigns the values to operation variable and also creates a string entered by the user
function clicked(a:String) {
    text.value += a;
    if(a=="+") {
        operation = "+";              //tells calculator addition is to be performed
    }
    else if(a=="-"){
        operation = "-";              //tells calculator substraction is to be performed 
    }
    else if(a=="*"){
        operation = "*";              //tells calculator multiply is to be performed
    }
    else if(a=="/"){
        operation = "/";              //tells calculator divide is to be performed
    }
}

//Solves the expression Entered by the user
function solve() {
    if(operation == "+"){
        var a = text.value.split("+",2);
        var num1: number = parseFloat(a[0]);
        var num2: number = parseFloat(a[1]);
        var result: number = num1 + num2;               //addition
        text.value = result.toString();
    }
    else if(operation == "-"){
        var a = text.value.split("-",2);
        var num1: number = parseFloat(a[0]);
        var num2: number = parseFloat(a[1]);
        var result: number = num1 - num2;               //substract
        text.value = result.toString();
    }
    else if(operation == "*"){
        var a = text.value.split("*",2);
        var num1: number = parseFloat(a[0]);
        var num2: number = parseFloat(a[1]);
        var result: number = num1 * num2;               //multiply
        text.value = result.toString();
    }
    else if(operation == "/"){
        var a = text.value.split("/",2);
        var num1: number = parseFloat(a[0]);
        var num2: number = parseInt(a[1]);
        var result: number = num1 / num2;               //divide
        text.value = result.toString();
    }
}