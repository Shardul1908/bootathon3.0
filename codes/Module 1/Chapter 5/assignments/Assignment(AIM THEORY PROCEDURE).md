### AIM:
#### _To learn Bresenham Circle Drawing Algorithm._
### THEORY:        
As the circle is collection of points that are at equal distance from the center of the circle. There are various methods to find the points on the circumference of the circle with respect to the center of the circle. The different methods are listed as below: 
1. Trigonometric Method :
	x=rcos&theta; and y=rsin &theta;
1. Polynomial Method :
	x2+y2=r2
1. Midpoint Circle Algorithm
1. Bresenham Circle Algorithm<br>
![theoryImage1](theoryImage1.png "theory image 1")
1. As shown in the figure1 the spacing between plotted pixel positions is not uniform.Bresenham Circle Algorithm plots the first pixel which lie on the Y axis with the coordinates as (0, Radius).
1. Bresenham Circle Algorithm makes the use of decision parameter to calculate the next pixel location from a previously known pixel location (x, y).
1. In Bresenham&rsquo;s algorithm at any point (x, y) we have two option to choose the next pixel in the X&ndash;direction either (x+1, y) or (x+1, y-1).
1. Consider the case explained below:
    <br>
    ![theoryimage2](theoryImage2.png "theory image 2")
1. If the point P1 is far away from actual circle;it will yield greater +ve value Da
i.e Da = x^2 + y^2 - R^2
1. If (Da +Db&gt;=0); then point P2 is closer to Actual Circle else P1 is closer     
1. If the point P2 is closer to the actual circle, then the x will be incremented and y will be decremented. If the point P1 is closer to the actual circle, then the x will be incremented and y will remain same.    
1. With the use of Bresenham circle drawing algorithm computation can be reduced by considering the symmetry of circles.     
1. 8 Way Symmetry:
    A point (a,b)can be represented with the use of 8 way symmetry as
    <br>
    ![theoryimage3](theoryImage3.png "theory image 3")
1. (a ,b); (a,-b); (-a ,b); (-a,-b) and (b, a); (b,-a); (-b, a); (-b,-a);
        
### PROCEDURE:
1. Start.
2. Accept center (xc,yc) point and radius r of the circle
3. Initialise:  
        x = 0  
        y = r
4. Calculate initial decision parameter as:  
        d = 3 &ndash; 2r
5. while(x&lt;=y)  
        {  
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Plotpoint(x,y); //use 8-way symmetry  
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if d&lt; 0  
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;then  
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;x++;  
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; y = y;  
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d = d + 4x + 6;  
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;else  
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; x++;  
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; y = y &ndash; 1;  
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; d = d + 4(x &ndash; y) + 10  
        }
6. Stop.
